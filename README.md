#  :rocket: Spring Security 登录插件
支持普通登录、验证码登录、小程序登录、标准OAuth2三方登录、非标准OAuth2登录。灵活、无侵入、可插拔，必须Star一个 ！
![](./loginPage.png)

## DEMO样例
参见相关的示例 [samples](https://gitee.com/felord/spring-security-login-extension/tree/master/sample)

## 使用方法
>  :tw-1f4cc: 这个仅仅作为 **Spring Security** 的一个扩展，不会侵入你原有的权限体系。你需要引入**Spring Boot Starter Security**。 

自行使用Maven命令`mvn install`到本地仓库，然后引入：
```xml
        <dependency>
            <groupId>cn.felord</groupId>
            <artifactId>spring-security-extension</artifactId>
            <version>1.0.0</version>
        </dependency>
```
然后参考**sample**项目进行开发。
## 相关知识
- 个人博客  [码农小胖哥的博客:https://www.felord.cn/](https://www.felord.cn/)
- 公众号：码农小胖哥  
![](./qr.jpg)
### 福利
关注公众号：码农小胖哥  回复 **2021开工福利** 和 **2022开工福利** 获取原创50万字的PDF。
### OAuth2 系列教程
[Spring Security 与 OAuth2](https://blog.csdn.net/qq_35067322/category_11691173.html)专题教程

## 登录方式
登录方式有三种。
### 普通登录

```http request
POST /login?username=user&password=12345 HTTP/1.1
Host: localhost:8085
```
### 验证码登录
> 需要先实现必须的配置接口

发送验证码后调用验证码登录接口：
```http request
POST /login/captcha?phone=11111111111&captcha=123123 HTTP/1.1
Host: localhost:8080
```
### 小程序登录
> 需要先实现必须的配置接口

前端先调用微信授权登录接口获取`openid`:
```http request
POST /miniapp/preauth?clientId=wxxda23234&jsCode=051A23234ZHa1tZ5yj3AOlFr HTTP/1.1
Host: localhost:8080
```
响应：
```json
{
    "code": 200,
    "data": {
        "errcode": null,
        "errmsg": null,
        "sessionKey": null,
        "openid": "oWmZj5QBrZxxxxx8OUxRrZJi4",
        "unionid": "oS-dxxxxxx4w_x7dA-h9MIuA"
    },
    "msg": "",
    "identifier": true
}
```
然后调用小程序登录接口：
```http request
POST /login/miniapp HTTP/1.1
Host: localhost:8080
Content-Type: application/json

{
    "clientId": "wxd14qr6",
    "openId": "oWmZj5QBrZIBks0xx8OUxRrZJi4",
    "unionId": "oS-dK520tgW8xxxx7dA-h9MIuA",
    "iv":"LQUOt8BSTa7xxxpe1Q==",
    "encryptedData": "10hn3o4xxxxxrO/Ag5nRD3QkLSzduKnWuzN9B/H4Y0G5mDPR8siA7T8yaaqZsrMycLAoe2qrd1J75yYetYuWifiq3jUrcceRZHVxxl9LnQdW8f5+pMTnQtCYiMJ7Jm9paCw2Bh+5Lowkyqkx1q0fALvCQ9LXPPLAbLOB9CavRfKoenAmyyHQjZ/6lz0njzA=="
}
```
### OAuth2 三方登录
标准OAuth2接入按照Spring Security OAuth2的配置即可，非标准的OAuth2目前支持企业微信、微信、微信网页授权，在配置信息准备妥当的前提下可30秒内集成完毕。
```java
   httpSecurity.apply(new OAuth2ProviderConfigurer(delegateClientRegistrationRepository))
                // 微信网页授权  下面的参数是假的
                .wechatWebclient("wxdf90xxx8e7f", "bf1306baaaxxxxx15eb02d68df5")
                // 企业微信登录 下面的参数是假的
                .workWechatWebLoginclient("wwa70dc5b6e56936e1", "nvzGI4Alp3xxxxxxZUc3TtPtKbnfTEets5W8", "1000005")
                // 微信扫码登录 下面的参数是假的
                .wechatWebLoginclient("wxafd62c05779e50bd", "ab24fce07ea84228dc4e64720f8bdefd");
```
### 微信网页授权
微信网页授权可使用沙盒测试，准备好账号和内网穿透就行了。具体参考[微信网页授权登录](https://felord.blog.csdn.net/article/details/123538976)
### 企业微信扫码
企业微信扫码，准备好企业微信管理账号并建立应用，可修改hosts文件来测试域名，也可以内网穿透进行域名测试。具体参考[Spring Security OAuth2整合企业微信扫码登录](https://mp.weixin.qq.com/s/XA2hCXfDBDosqlNvyEz9Dg)
### 微信扫码登录
需要微信开放平台以及在开放平台创建网页应用，需要修改hosts文件来测试域名，测试时应该使用`80`端口。
>具体参考项目内DEMO
## JWT

### JWT 密钥证书

利用Keytool工具生成，采用RSA算法，以前文章有介绍，自己找找。